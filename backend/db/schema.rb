# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_05_235401) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.bigint "media_id"
    t.string "url"
    t.string "title"
    t.string "color"
    t.float "sentiment"
    t.boolean "selected"
    t.boolean "selectable"
    t.boolean "locked"
    t.boolean "grabbable"
    t.integer "x"
    t.integer "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["media_id"], name: "index_articles_on_media_id"
  end

  create_table "edges", force: :cascade do |t|
    t.bigint "personne_id"
    t.bigint "media_id"
    t.bigint "institution_id"
    t.bigint "job_id"
    t.bigint "article_id"
    t.bigint "partie_id"
    t.datetime "date"
    t.string "edge_type"
    t.string "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_edges_on_article_id"
    t.index ["institution_id"], name: "index_edges_on_institution_id"
    t.index ["job_id"], name: "index_edges_on_job_id"
    t.index ["media_id"], name: "index_edges_on_media_id"
    t.index ["partie_id"], name: "index_edges_on_partie_id"
    t.index ["personne_id"], name: "index_edges_on_personne_id"
  end

  create_table "institutions", force: :cascade do |t|
    t.string "name"
    t.string "inst_type"
    t.string "color"
    t.boolean "selected"
    t.boolean "selectable"
    t.boolean "locked"
    t.boolean "grabbable"
    t.integer "x"
    t.integer "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "jobs", force: :cascade do |t|
    t.string "name"
    t.string "job_type"
    t.boolean "is_public"
    t.string "color"
    t.boolean "selected"
    t.boolean "selectable"
    t.boolean "locked"
    t.boolean "grabbable"
    t.integer "x"
    t.integer "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "media", force: :cascade do |t|
    t.string "name"
    t.string "color"
    t.boolean "selected"
    t.boolean "selectable"
    t.boolean "locked"
    t.boolean "grabbable"
    t.integer "x"
    t.integer "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parties", force: :cascade do |t|
    t.string "name"
    t.string "orientation"
    t.datetime "date"
    t.string "color"
    t.boolean "selected"
    t.boolean "selectable"
    t.boolean "locked"
    t.boolean "grabbable"
    t.integer "x"
    t.integer "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personnes", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "wealth"
    t.boolean "millionaire"
    t.string "convicted"
    t.string "avatar_url"
    t.string "color"
    t.boolean "selected"
    t.boolean "selectable"
    t.boolean "locked"
    t.boolean "grabbable"
    t.integer "x"
    t.integer "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sources", force: :cascade do |t|
    t.bigint "edge_id"
    t.bigint "personne_id"
    t.string "url"
    t.string "type"
    t.integer "valid"
    t.integer "invalid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["edge_id"], name: "index_sources_on_edge_id"
    t.index ["personne_id"], name: "index_sources_on_personne_id"
  end

end
