class CreateSources < ActiveRecord::Migration[5.2]
  def change
    create_table :sources do |t|
      t.belongs_to :edge
      t.belongs_to :personne
      t.string :url
      t.string :type
      t.integer :valid
      t.integer :invalid

      t.timestamps
    end
  end
end
