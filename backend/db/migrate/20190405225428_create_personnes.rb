class CreatePersonnes < ActiveRecord::Migration[5.2]
  def change
    create_table :personnes do |t|
      t.string :first_name
      t.string :last_name
      t.string :wealth
      t.boolean :millionaire
      t.string :convicted
      t.string :avatar_url
      t.string :color

      # Cytoscape node info
      t.boolean :selected
      t.boolean :selectable
      t.boolean :locked
      t.boolean :grabbable
      t.integer :x
      t.integer :y
      t.timestamps
    end
  end
end
