class CreateEdges < ActiveRecord::Migration[5.2]
  def change
    create_table :edges do |t|
      t.belongs_to :personne, index: true
      t.belongs_to :media, index: true
      t.belongs_to :institution, index: true
      t.belongs_to :job, index: true
      t.belongs_to :article, index: true
      t.belongs_to :partie, index: true
      t.datetime :date
      t.string :edge_type
      t.string :color

      t.timestamps
    end
  end
end
