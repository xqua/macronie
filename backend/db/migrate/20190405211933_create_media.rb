class CreateMedia < ActiveRecord::Migration[5.2]
  def change
    create_table :media do |t|
      t.string :name
      t.string :color
      # Cytoscape node info
      t.boolean :selected
      t.boolean :selectable
      t.boolean :locked
      t.boolean :grabbable
      t.integer :x
      t.integer :y
      t.timestamps
    end
  end
end
