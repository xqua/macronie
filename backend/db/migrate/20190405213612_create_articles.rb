class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.belongs_to :media
      t.string :url
      t.string :title
      t.string :color
      
      t.float :sentiment
      # Cytoscape node info
      t.boolean :selected
      t.boolean :selectable
      t.boolean :locked
      t.boolean :grabbable
      t.integer :x
      t.integer :y

      t.timestamps
    end
  end
end
