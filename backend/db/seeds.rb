# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or find_or_create_byd alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.find_or_create_by([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.find_or_create_by(name: 'Luke', movie: movies.first)
Personne.find_or_create_by(first_name: "Emmanuel", last_name:"Macron", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Edouard", last_name:"Philippe", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Ludovic", last_name:"Chaker", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Alexandre", last_name:"Benalla", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Bernard", last_name:"Arnault", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Bernard", last_name:"Squarcini", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Jean-Pierre", last_name:"Jouyet", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Henry", last_name:"Hermand", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Xavier", last_name:"Niel", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Michele", last_name:"Marchand", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Jean-Michel", last_name:"Decugis", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Pauline", last_name:"Guena", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Marc", last_name:"Leplongeon", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Renaud", last_name:"Van Ruymbeke", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Louis", last_name:"Dreyfus", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Martin", last_name:"Bouygues", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Raphaëlle", last_name:"Bacqué", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Anne", last_name:"Hidalgo", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Jean-Louis", last_name:"Missika", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Christophe", last_name:"Girard", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Brigitte", last_name:"Macron", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Arnaud", last_name:"Lagardère", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Ramzy", last_name:"Khiroun", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Nicolas", last_name:"Sarkozy", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Francois", last_name:"Fillion", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Laurent", last_name:"Delahousse", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Delphine", last_name:"Ernotte", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "David", last_name:"Pujadas", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Marc", last_name:"Endeweld", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Delphine", last_name:"Arnault", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Edwy", last_name:"Plenel", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Laurent", last_name:"Mauduit", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Daniel", last_name:"Kretinsky", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Patrick", last_name:"Drahi", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Laurent", last_name:"Joffrin", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Bernard", last_name:"Mourad", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Laurent", last_name:"Fabius", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Olivier", last_name:"Dassault", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Vincent", last_name:"Bolloré", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Herve", last_name:"Gagnetto", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Francois", last_name:"Pinault", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Alain", last_name:"Weill", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Virginie", last_name:"Malingre", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Ariane", last_name:"Chemin", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Francoise", last_name:"Nyssen", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Sebastien", last_name:"Lecornu", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Ismael", last_name:"Emelien", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Stephan", last_name:"Sejourné", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Dominique", last_name:"Strauss-Kahn", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Richard", last_name:"Descoing", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Edith", last_name:"Chabre", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Brigitte", last_name:"Taittainger-Jouyet", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Francois", last_name:"Hollande", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Jacques", last_name:"Attali", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Edith", last_name:"Chabre", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Nadia", last_name:"Marik", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Guillaume", last_name:"Pepy", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Laurent", last_name:"Bigorgne", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Maurice", last_name:"Levy", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Emmanuelle", last_name:"Wargon", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Patricia", last_name:"Barbizet", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Frederic", last_name:"Mion", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Catherine", last_name:"Grenier-Weill", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Jean-Michel", last_name:"Blanquer", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Anne", last_name:"Lauvergeon", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Muriel", last_name:"Penicaud", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Gilles", last_name:"Finchekstein", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Jean-Yves", last_name:"Le Drian", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Gabriel", last_name:"Attal", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Benjamin", last_name:"Griveaux", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Eric", last_name:"Fottorina", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Marc-Olivier", last_name:"Padis", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Bruno", last_name:"Tertais", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Christophe", last_name:"Barbier", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Franz-Olivier", last_name:"Giesbert", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Pierre", last_name:"de Panafieu", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Juan", last_name:"Branco", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Stanislas", last_name:"Guerini", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Yves", last_name:"Attal", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Francis", last_name:"Bouygues", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Jean-Claude", last_name:"Fleury", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Alain", last_name:"Touraine", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Marisol", last_name:"Touraine", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Alexandra", last_name:"Touraine", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Segolene", last_name:"Royal", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Frederic", last_name:"Mitterrand", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Pierre", last_name:"Moscovici", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Pierre", last_name:"Person", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Bernard", last_name:"Cazeneuve", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "Quentin", last_name:"Lafay", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
Personne.find_or_create_by(first_name: "André", last_name:"Santini", wealth: 100000, millionaire: true, convicted:false, avatar_url: "")
# Petite fille de Giscard d’Estaing
# Fille du PDG Club Med
# Fille du PDG d’arches
# Héritier Seudoux
# Fraterie Godot
# Héritier Hautelocque
# Fille de Bernard Zekri
# Fille de Jean Toutou
# Petit Fils de Michel Pebereau
# Fille de Gerardo Della Paolera
# Fils Proviseur H4
# Héritière Olivennes





Media.find_or_create_by(name: "Le Parisien")
Media.find_or_create_by(name: "Les Echos")
Media.find_or_create_by(name: "Le Monde")
Media.find_or_create_by(name: "Gallimard")
Media.find_or_create_by(name: "Flamarion")
Media.find_or_create_by(name: "Radio Classique")

Job.find_or_create_by(name: "President de la republique", job_type: "Politique", is_public: true)
Job.find_or_create_by(name: "Premier Ministre", job_type: "Politique", is_public: true)
Job.find_or_create_by(name: "Banquier d'affaires", job_type: "Finance", is_public: false)
Job.find_or_create_by(name: "PDG", job_type: "Cadre", is_public: false)
Job.find_or_create_by(name: "Avocat", job_type: "Justice", is_public: false)

Institution.find_or_create_by(name: "SciencePo", inst_type: "Ecole")
Institution.find_or_create_by(name: "ENA", inst_type: "Ecole")
Institution.find_or_create_by(name: "Areva", inst_type: "Entreprise")
Institution.find_or_create_by(name: "Polytechnique", inst_type: "Ecole")
Institution.find_or_create_by(name: "LVMH", inst_type: "Groupe Privee")
Institution.find_or_create_by(name: "Rotschild", inst_type: "Banque")

Partie.find_or_create_by(name: 'UMP', orientation: "Droite")
Partie.find_or_create_by(name: 'LR', orientation: "Droite")
Partie.find_or_create_by(name: 'RPR', orientation: "Droite")
Partie.find_or_create_by(name: 'PS', orientation: "Gauche")

# Emmanuel Macron
personne = Personne.where(first_name: "Emmanuel", last_name: "Macron").first
personne.jobs << Job.where(name: "President de la republique").first
personne.jobs << Job.where(name: "Banquier d'affaires").first
personne.institutions << Institution.where(name: "Rotschild")
personne.institutions << Institution.where(name: "SciencePo")
personne.institutions << Institution.where(name: "ENA")

# Edouard Philippe
personne = Personne.where(first_name: "Edouard", last_name: "Philippe").first
personne.jobs << Job.where(name: "Premier Ministre").first
personne.jobs << Job.where(name: "Avocat").first
personne.institutions << Institution.where(name: "SciencePo")
personne.institutions << Institution.where(name: "ENA")
personne.institutions << Institution.where(name: "Areva")

#Xavier Niel
personne = Personne.where(first_name: "Xavier", last_name: "Niel").first
personne.medias << Media.where(name: "Le Monde").first
personne.jobs << Job.where(name: "PDG").first

# Bernard Arnault
personne = Personne.where(first_name: "Bernard", last_name: "Arnault").first
personne.medias << Media.where(name: "Le Parisien").first
personne.medias << Media.where(name: "Les Echos").first
personne.medias << Media.where(name: "Gallimard").first
personne.medias << Media.where(name: "Flamarion").first
personne.medias << Media.where(name: "Radio Classique").first
personne.jobs << Job.where(name: "PDG").first
