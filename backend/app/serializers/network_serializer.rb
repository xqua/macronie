class NetworkSerializer < ActiveModel::Serializer
  attributes :elements

  def elements
    nodes = []
    object.data.map do |n|
      nodes << node(n)
    end
    edges = []
    Edge.all.map do |e|
      edges << e
    end
    elements = {
      nodes: nodes,
      edges: edges,
      layout: {
        name: 'grid',
        rows: 1
      },
      style: [
        {
      selector: 'node',
      style: {
        'label': 'data(name)'
      }
      }
      ]
    }
    return elements
  end


  def edge(e)
    association = []
    if e.personne_id?
      association << 'personne_' + e.personne_id
    end
    if e.media_id?
      association << 'media_' + e.media_id
    end
    if e.institution_id?
      association << 'institution_' + e.institution_id
    end
    if e.job_id?
      association << 'job_' + e.job_id
    end
    if e.article_id?
      association << 'article_' + e.article_id
    end
    if e.partie_id?
      association << 'partie_' + e.partie_id
    end
    edge = {
      data: {
        id: association.compact.join('_'),
        source: association[0],
        target: association[1],
        date: e.date
      }
    }
  end

  def node(n)
    node = {
      data: {
        id: "id",
        classes: [],
        selected: n.selected,
        selectable: n.selectable,
        locked: n.locked,
        grabbable: n.grabbable
      },
      position: {
        x: n.x,
        y: n.y
      }
    }
    type = n.class.name
    if type == 'Personne'
      node.data.id = 'personne_' + n.id
      node.data['first_name'] = n.first_name
      node.data['last_name'] = n.last_name
      node.data['name'] = [n.first_name, n.last_name].compact.join(' ')
      node.data['millionaire'] = n.millionaire
      node.data['convicted'] = n.convicted
      node.data['wealth'] = n.wealth
      node.data['image_url'] = n.avatar_url
      node.classes << "personne"
    elsif type == "Media"
      node.data.id = 'media_' + n.id
      node.data['name'] = n.name
      node.classes << "media"
    elsif type == "Job"
      node.data['name'] = n.name
      node.data['job_type'] = n.job_type
      node.classes << "job"
    elsif type == "Institution"
      node.data['name'] = n.name
      node.data['institution_type'] = n.inst_type
      node.classes << "institution"
    elsif type == "Partie"
      node.data['name'] = n.name
      node.data['orientation'] = n.orientation
      node.classes << "partie"
    elsif type == 'Article'
      node.data['media'] = n.media
      node.data['url'] = n.url
      node.data['title'] = n.title
      node.data['sentiment'] = n.sentiment
      node.classes << "article"
    end
    return node
  end
end
