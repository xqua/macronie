class NetworkController < ApplicationController
  def index
    @peoples = Personne.all.to_a
    @institutions = Institution.all.to_a
    @jobs = Job.all.to_a
    @medias = Media.all.to_a
    @articles = Article.all.to_a
    @parties = Partie.all.to_a
    @network = @peoples + @institutions + @medias + @articles + @jobs + @parties
    json = elements(@network)
    render json: json, status: :ok
  end

  def elements(network)
    nodes = []
    network.map do |n|
      nodes << node(n)
    end
    edges = []
    Edge.all.map do |e|
      edges << edge(e)
    end
    elements = {
      nodes: nodes,
      edges: edges,
      layout: {
        name: 'grid',
        rows: 1
      },
      style: [
        {
      selector: 'node',
      style: {
        'label': 'data(name)'
      }
      }
      ]
    }
    return elements
  end


  def edge(e)
    association = []
    if e.personne_id?
      association << 'personne_' + e.personne_id.to_s
    end
    if e.media_id?
      association << 'media_' + e.media_id.to_s
    end
    if e.institution_id?
      association << 'institution_' + e.institution_id.to_s
    end
    if e.job_id?
      association << 'job_' + e.job_id.to_s
    end
    if e.article_id?
      association << 'article_' + e.article_id.to_s
    end
    if e.partie_id?
      association << 'partie_' + e.partie_id.to_s
    end
    edge = {
      data: {
        id: association.compact.join('_'),
        source: association[0],
        target: association[1],
        color: e.color,
        date: e.date
      }
    }
  end

  def node(n)
    node = {
      data: {
        id: "id",
        color: n.color,
        classes: [],
        selected: n.selected,
        selectable: n.selectable,
        locked: n.locked,
        grabbable: n.grabbable
      },
      position: {
        x: n.x,
        y: n.y
      }
    }
    type = n.class.name
    if type == 'Personne'
      node[:data][:id] = 'personne_' + n.id.to_s
      node[:data][:first_name] = n.first_name
      node[:data][:last_name] = n.last_name
      node[:data][:name] = [n.first_name, n.last_name].compact.join(' ')
      node[:data][:millionaire] = n.millionaire
      node[:data][:convicted] = n.convicted
      node[:data][:wealth] = n.wealth
      node[:data][:image_url] = n.avatar_url
      node[:data][:classes] << "personne"
    elsif type == "Media"
      node[:data][:id] = 'media_' + n.id.to_s
      node[:data][:name] = n.name
      node[:data][:classes] << "media"
    elsif type == "Job"
      node[:data][:id] = 'job_' + n.id.to_s
      node[:data][:name] = n.name
      node[:data][:job_type] = n.job_type
      node[:data][:classes] << "job"
    elsif type == "Institution"
      node[:data][:id] = 'institution_' + n.id.to_s
      node[:data][:name] = n.name
      node[:data][:institution_type] = n.inst_type
      node[:data][:classes] << "institution"
    elsif type == "Partie"
      node[:data][:id] = 'partie_' + n.id.to_s
      node[:data][:name] = n.name
      node[:data][:orientation] = n.orientation
      node[:data][:classes] << "partie"
    elsif type == 'Article'
      node[:data][:id] = 'article_' + n.id.to_s
      node[:data][:media] = n.media
      node[:data][:url] = n.url
      node[:data][:title] = n.title
      node[:data][:sentiment] = n.sentiment
      node[:data][:classes] << "article"
    end
    return node
  end
end
