class Edge < ApplicationRecord
  has_many :sources
  belongs_to :media, optional: true
  belongs_to :article, optional: true
  belongs_to :personne, optional: true
  belongs_to :institution, optional: true
  belongs_to :job, optional: true
  belongs_to :partie, optional: true
end
