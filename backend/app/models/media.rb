class Media < ApplicationRecord
  has_many :edges
  has_many :personnes, :through => :edges
  has_many :institutions, :through => :edges
  has_many :articles, :through => :edges
  after_initialize :default_values

  def default_values
    self.selected = false
    self.selectable = true
    self.locked = false
    self.grabbable = true
    self.x = 0
    self.y = 0
    self.color = "#55a868"
  end
end
