class Partie < ApplicationRecord
  has_many :edges
  has_many :personnes, :through => :edges
  after_initialize :default_values

  def default_values
    self.selected = false
    self.selectable = true
    self.locked = false
    self.grabbable = true
    self.x = 0
    self.y = 0
    self.color = "#dd8452"
  end
end
