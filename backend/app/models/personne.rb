class Personne < ApplicationRecord
  has_many :edges
  has_many :institutions, :through => :edges
  has_many :medias, :through => :edges
  has_many :jobs, :through => :edges
  after_initialize :default_values

  def default_values
    self.selected = false
    self.selectable = true
    self.locked = false
    self.grabbable = true
    self.x = 0
    self.y = 0
    self.color = "#4c72b0"
  end
end
