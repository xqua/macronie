json.extract! media, :id, :created_at, :updated_at
json.url media_url(media, format: :json)
