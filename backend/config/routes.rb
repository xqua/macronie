Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :network, only: [:index] do
  end
  # resources :media,                   :defaults => { :format => :json }
  # resources :personnes,                   :defaults => { :format => :json }
  # resources :articles,                   :defaults => { :format => :json }
  # resources :sources,                   :defaults => { :format => :json }
  # resources :institutions,                   :defaults => { :format => :json }
  # resources :edges,                   :defaults => { :format => :json }
  # resources :jobs,                   :defaults => { :format => :json }
  # resources :parties,                   :defaults => { :format => :json }
  resources :personnes
end
