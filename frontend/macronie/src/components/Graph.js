import Api from '../Api.js'
import CytoscapeComponent from 'react-cytoscapejs';
import React, { Component } from 'react';
import Infos from './Infos.js'
import Cytoscape from 'cytoscape';
import Cola from 'cytoscape-cola';
import { Grid, Button } from 'semantic-ui-react'


Cytoscape.use(Cola);

export default class Graph extends Component {
  constructor(props) {
  		super(props);
  		this.state = {
  			network: null,
  			errors: "",
  			success: false,
        cyGraph: null,
        logs: "",
        selectedNode: {
          "id": 1,
          "first_name": "temp first",
          "last_name": "temps last",
          "name": "a name"
        },
        selectedEdge: {
          "id": 1,
          "name": "bla"
        }
  		};
      this.getLog = this.getLog.bind(this);
      this.setNode = this.setNode.bind(this);
  	};

  componentDidMount(){
     Api.get("/network").then(res=>{
       console.log("Newtwork got : ", res.data);
       this.setState({network: res.data, success: true});
     }).catch(error=>{
       console.log("ERR : ",error);
     })
     // this.setState({cyGraph: "graph"});
     console.log(this.state.cyGraph);
   }

   componentDidUpdate() {
     this.state.cyGraph.on('tap', 'node', this.setNode);
   }

   setNode(evt) {
     console.log(evt.target.data()['name']);
     this.setState({selectedNode: evt.target.data()});
     console.log(this.state.selectedNode.name);
   }

   getLog() {

    //  this.state.cyGraph.on('tap', 'node', function (evt) {
    //      console.log(evt.target.data()['name']);
    //      this.state.selectedNode = evt.target.data();
    // });
    var data = this.state.cyGraph.$(':selected').json();
    if (data) {
      console.log(data['data']);
      // this.setState({logs: data['data']})
    }
    }

 render(){



     const network = this.state.network;
     var logs = this.state.logs;
     const layout = { name: 'cola' };
     const style = {
                     width: '100%',
                     height: '600px',

                   };

     const stylesheet= [
                        {
                          selector: 'node',
                          style: {
                            "width": 20,
                            "height": 20,
                            "label": "data(name)",
                            "background-color": "data(color)",
                            "font-size": "12px",
                            "text-valign": "center",
                            "text-halign": "center",
                          }
                        },
                        {
                          selector: 'edge',
                          style: {
                            width: 15
                          }
                        }
                      ];

     var cyGraph = this.state.cyGraph;

     const { error, success } = this.state;
     if (error) {
     return <div>Error: {error.message}</div>;
   } else if (!success) {
     return <div>Loading...</div>;
   } else {
     return(
       <div>
       <Grid>
        <Grid.Row>
         <Grid.Column  floated='left' width={10}>
          <CytoscapeComponent cy={cy => this.state.cyGraph = cy} autounselectify={false} elements={CytoscapeComponent.normalizeElements(this.state.network)} style={ style } stylesheet={stylesheet} layout={layout} minZoom={0.3} maxZoom={2}/>
         </Grid.Column>
         <Grid.Column  floated='right' width={4}>
          <Infos node={this.state.selectedNode}/>
         </Grid.Column>
        </Grid.Row>
       </Grid>
       <Button onClick={this.getLog}>Get Logs</Button>
       </div>
     )
     }

   }
}
