import React, { Component } from 'react';

export default class Infos extends Component {
  constructor(props) {
  		super(props);
      this.state = {
        node: this.props.node,
      };
  	};

    static get defaultProps() {
      return {
        node: {
          "id": null,
          "first_name": "",
          "last_name": "",
          "name": ""
        },
      }
    }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.node !== prevProps.node) {
      this.setState({node: this.props.node});
    }
  }

 render(){
    const node = this.state.node;
     return(
       <div className="test">
       <p>{node.id}</p>
       <p>{node.name}</p>
       <p>{node.first_name}</p>
       <p>{node.last_name}</p>
       </div>
     )
     }

}
