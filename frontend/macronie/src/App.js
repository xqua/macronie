import React, { Component } from 'react';
import logo from './logo.svg';
import ReactDOM from 'react-dom';
import './App.css';
import Graph from './components/Graph.js'
import { Header } from 'semantic-ui-react'

class App extends Component {
  constructor(props){
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: []
      };
    }

  render() {
    // const elements = [
    //        { data: { id: 'one', label: 'Node 1' }, position: { x: 0, y: 0 } },
    //        { data: { id: 'two', label: 'Node 2' }, position: { x: 100, y: 0 } },
    //        { data: { source: 'one', target: 'two', label: 'Edge from Node1 to Node2' } }
    //     ];

      return (
        <div className="App">
          <header className="App-header">
          <Header as='h1'>La Macronie</Header>
          <p>
          Explore et comprends les reseaux de pouvoirs qui ce cache deriere ce gouvernement.
          </p>
          </header>
          <Graph />
        </div>
      );

  }
}
//
// class Cytoscape extends React.Component {
//   constructor(props){
//     super(props);
//   }
//
//   render(){
//     const elements = [
//        { data: { id: 'one', label: 'Node 1' }, position: { x: 0, y: 0 } },
//        { data: { id: 'two', label: 'Node 2' }, position: { x: 100, y: 0 } },
//        { data: { source: 'one', target: 'two', label: 'Edge from Node1 to Node2' } }
//     ];
//
//     return <CytoscapeComponent elements={elements} style={ { width: '600px', height: '600px' } } />;
//   }
// }
//
// ReactDOM.render( React.createElement(Cytoscape, document.getElementById('root')));

export default App;
