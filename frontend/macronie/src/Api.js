import axios from 'axios';

// var url = process.env.REACT_APP_ADDRESS_BACK;
var url = "http://localhost:3000";

const api = axios.create({
//  baseURL: process.env.BACKEND_URL,
  baseURL: url,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
});

export default api;
